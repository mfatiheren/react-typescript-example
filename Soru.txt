Bankanın şubeleri gösteren bir usercontrol yapılmalıdır.
Bu usercontrolün yapılmasının amacı kodun tekrar tekrar kullanılması ve tüm ekranlarda standart bir kullanıcı deneyimi elde edilmesidir.
Bunun için alttaki xml bir comboboxa yüklenmelidir.
Combo boxtaki satırlarda şubekodu ve adı görülmelidir.
Seçilen şube için ilgi parent form şube adına, koduna kolonlara erişebilmelidir.
Kullanılacak Yazılım dilleri typescript ve react teknolojileridir.
Süre 10 gün.
Sonucu public github hesabına commit yapıp iletmenizi talep ediyoruz.

<NewDataSet>
  <xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
    <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
      <xs:complexType>
        <xs:choice minOccurs="0" maxOccurs="unbounded">
          <xs:element name="SUBELER">
            <xs:complexType>
              <xs:sequence>
                <xs:element name="SubeKd" type="xs:string" minOccurs="0" />
                <xs:element name="SubeAdi" type="xs:string" minOccurs="0" />
              </xs:sequence>
            </xs:complexType>
          </xs:element>
        </xs:choice>
      </xs:complexType>
    </xs:element>
  </xs:schema>
  <SUBELER>
    <SubeKd>1</SubeKd>
    <SubeAdi>MERKEZ/ANKARA ŞUBESİ</SubeAdi>
  </SUBELER>
  <SUBELER>
    <SubeKd>2</SubeKd>
    <SubeAdi>OLTU/ERZURUM ŞUBESİ</SubeAdi>
</SUBELER>
  <SUBELER>
    <SubeKd>4</SubeKd>
    <SubeAdi>BALA/ANKARA ŞUBESİ</SubeAdi>
   </SUBELER>
  <SUBELER>
    <SubeKd>5</SubeKd>
    <SubeAdi>BEYPAZARI/ANKARA ŞUBESİ</SubeAdi>  
</SUBELER>
  <SUBELER>
    <SubeKd>6</SubeKd>
    <SubeAdi>ÇUBUK/ANKARA ŞUBESİ</SubeAdi>
</SUBELER>
</NewDataSet>
