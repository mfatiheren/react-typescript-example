import React, { Component } from "react";
import Select from 'react-select'
import Xml2js from 'xml2js';

let branchesXml = `<NewDataSet>
<xs:schema id="NewDataSet" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:msdata="urn:schemas-microsoft-com:xml-msdata">
  <xs:element name="NewDataSet" msdata:IsDataSet="true" msdata:UseCurrentLocale="true">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="SUBELER">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="SubeKd" type="xs:string" minOccurs="0" />
              <xs:element name="SubeAdi" type="xs:string" minOccurs="0" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
  </xs:element>
</xs:schema>
<SUBELER>
  <SubeKd>1</SubeKd>
  <SubeAdi>MERKEZ/ANKARA ŞUBESİ</SubeAdi>
</SUBELER>
<SUBELER>
  <SubeKd>2</SubeKd>
  <SubeAdi>OLTU/ERZURUM ŞUBESİ</SubeAdi>
</SUBELER>
<SUBELER>
  <SubeKd>4</SubeKd>
  <SubeAdi>BALA/ANKARA ŞUBESİ</SubeAdi>
 </SUBELER>
<SUBELER>
  <SubeKd>5</SubeKd>
  <SubeAdi>BEYPAZARI/ANKARA ŞUBESİ</SubeAdi>  
</SUBELER>
<SUBELER>
  <SubeKd>6</SubeKd>
  <SubeAdi>ÇUBUK/ANKARA ŞUBESİ</SubeAdi>
</SUBELER>
</NewDataSet>`;

type branch = { SubeKd: string, SubeAdi: string };
type option = { value: number, label: string };

type convertOptionsType = (xml: string) => option[];

let convertOption: convertOptionsType = (xml: string) => {
  let json: branch[] = [];
  Xml2js.parseString(xml, (err, result) => {
    if (err) {
      throw err;
    }
    json = result.NewDataSet.SUBELER;
  });
  return json.map(b => {
    let o: option = {
      value: Number(b.SubeKd),
      label: "0000".substring(0, 4 - b.SubeKd.length) + b.SubeKd + ' - ' + b.SubeAdi
    }
    return o;
  });
};

let options: option[] = convertOption(branchesXml);

class Branches extends Component {

  state: { selectedOption: option | null } = {
    selectedOption: null
  };

  handleChange = (selectedOption: any) => {
    this.setState({ selectedOption });
  };

  render() {
    const { selectedOption } = this.state;

    return (
      <div>
        <Select
          value={selectedOption}
          onChange={this.handleChange}
          placeholder="Şube Seçiniz"
          options={options}
        />
        <pre>
          <code>Şube Adı : {selectedOption?.label.split("-")[1].trim() ?? "???"}</code>
          <br />
          <code>Şube Kodu: {selectedOption?.value ?? "???"}</code>
        </pre>
      </div>
    );
  }
}
export default Branches;