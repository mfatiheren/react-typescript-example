import React from 'react';
import './App.css';
import Branches from './components/Branches';
function App() {
  
  return (
    <div className="App">
      <header className="App-header">
        <div className="Branches"><Branches /></div>
      </header>
    </div>
  );
}

export default App;
